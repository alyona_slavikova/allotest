﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
    public class Contacts
    {
        private IWebDriver _driver;
        public By ContactsMainLabel = By.XPath("/html/body/div[1]/header/div[1]/div/ul/li[8]/a");
        public By ContactsPageLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");
        

        public Contacts(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindContactsTitle()
        {
            return _driver.FindElement(ContactsMainLabel);
        }
        public string GetContactsTitleText()
        {
            return FindContactsTitle().Text;
        }

        public IWebElement FindContactsPageTitle()
        {
            return _driver.FindElement(ContactsPageLabel);
        }
        public string GetContactsPageTitleText()
        {
            return FindContactsPageTitle().Text;
        }

       
    }
}
