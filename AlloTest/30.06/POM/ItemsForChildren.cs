﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
   public class ItemsForChildren
    {
        private IWebDriver _driver;

        public By ItemForChildrenLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[2]/div/ul/li[13]/a/p");
        public By ChildrensPageLabel = By.XPath("/html/body/div[1]/div/div/div[2]/h1");


        public ItemsForChildren(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindGoodsForChildrenTitle()
        {
            return _driver.FindElement(ItemForChildrenLabel);
        }

        public string GetGoodsForChildrenTitleText()
        {
            return FindGoodsForChildrenTitle().Text;
        }

        public IWebElement FindChildrensPageTitle()
        {
            return _driver.FindElement(ChildrensPageLabel);
        }
        public string GetChildrensPageTitleText()
        {
            return FindChildrensPageTitle().Text;
        }

    }
}
