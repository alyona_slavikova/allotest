﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
    public class Credit
    {
        private IWebDriver _driver;

        public By CreditLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By CreditPageLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");
        public Credit(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindCreditLabelTitle()
        {
            return _driver.FindElement(CreditLabel);
        }

        public string GetCreditTitleText()
        {
            return FindCreditLabelTitle().Text;
        }

        public IWebElement FindCreditPageLabelTitle()
        {
            return _driver.FindElement(CreditPageLabel);
        }

        public string GetCreditPageTitleText()
        {
            return FindCreditPageLabelTitle().Text;
        }

    }
}
