﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
    public class PopUpCart
    {
        private IWebDriver _driver;
        public By mainLabel = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By linkToMainPage = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By labelWebinars = By.XPath("/html/body/main/div/div[2]/h1");
        
        public PopUpCart(IWebDriver driver)   
        {
           this._driver = driver;
        }
        
    }
}
