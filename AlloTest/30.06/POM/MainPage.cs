﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
   public class MainPage

    {
        private IWebDriver _driver;
        public By ButtonDarkTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[1]");
        public By ToggleThemeSwitcher = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/div");
        public By ButtonLightTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[2]");
        public By ButtonBlog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By ButtonFishka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By ButtonJobPositions = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By ButtonShops = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By ButtonDeliveryAndPay = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        
        public By ButtonContacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By WarrantyButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By MarkDownButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a/img");
        public By TVAudioPhotoButton = By.XPath("/html/body/div[1]/main/div/div/div[1]/div/div/div/ul/li[2]/a/span");


        public By mainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By CreditLabelBtn = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By CutInPriceLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a");
        public By CartButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        public By CartIsEmptyLabel = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By ContactsBtn = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By ItemsForChildrenBtn = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[2]/div/ul/li[13]/a/p");
        public MainPage(IWebDriver driver)
        {
            this._driver = driver;

        }

        public IWebElement FindCartLabel()
        {
            return _driver.FindElement(CartIsEmptyLabel);
        }

        public string GetTextFromCartIsEmptyLabel()
        {
            return FindCartLabel().Text;
        }

        public PopUpCart ClickOnCartButton()
        {
            _driver.FindElement(CartButton).Click();
            return new PopUpCart(_driver);
        }


        public Warrant ClickOnWarrantyButton()
        {
            _driver.FindElement(WarrantyButton).Click();
            return new Warrant(_driver);
        }

        public MarkDown ClickOnMarkDownBtn()
        {
            _driver.FindElement(MarkDownButton).Click();
            return new MarkDown(_driver);
        }

        public MarkDown ClickOnTVAudioPhotoBtn()
        {
            _driver.FindElement(TVAudioPhotoButton).Click();
            return new MarkDown(_driver);
        }

        public Contacts ClickOnContactsButton()
        {
            _driver.FindElement(ContactsBtn).Click();
            return new Contacts(_driver);
        }

        public ItemsForChildren ClickOnItemsForChildrenButton()
        {
            _driver.FindElement(ItemsForChildrenBtn).Click();
            return new ItemsForChildren(_driver);
        }

        public Credit ClickOnCreditButton()
        {
            _driver.FindElement(CreditLabelBtn).Click();
            return new Credit(_driver);
        }

    }
}
