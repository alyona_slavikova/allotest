﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
    public class MarkDown
    {
        private IWebDriver _driver;

        public By MarkDownButtonLabel = By.XPath("/html/body/div[4]/div[1]/div[2]/div[1]/div/div[1]/div[2]/div/ul/li[5]/a/p");
        public By MarkDownPageLabel = By.XPath("/html/body/div[1]/header/div[3]/div/h1");
        public By TVAudioPhotoMainLabel = By.XPath("/html/body/div[1]/main/div/div/div[1]/div/div/div/ul/li[2]/a/span");
        public By TVAudioPhotoLabel = By.XPath("/html/body/div[1]/main/div/div/div[2]/div[2]/div[1]/a[2]");

        public MarkDown(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindMarkDownTitle()
        {
            return _driver.FindElement(MarkDownButtonLabel);
        }

        public string FindMarkDownBtnText()
        {
            return FindMarkDownTitle().Text;
        }

        public IWebElement FindMarkDownPageTitle()
        {
            return _driver.FindElement(MarkDownPageLabel);
        }
        public string GetMarkDownBtnText()
        {
            return FindMarkDownPageTitle().Text;
        }

        public IWebElement FindTVAudioPhotoTitle()
        {
            return _driver.FindElement(TVAudioPhotoMainLabel);
        }

        public string GetTVAudioPhotoText()
        {
            return FindTVAudioPhotoTitle().Text;
        }

        public IWebElement FindTVAudioPhotoInnerTitle()
        {
            return _driver.FindElement(TVAudioPhotoLabel);
        }

        public string GetTVAudioPhotoInnerText()
        {
            return FindTVAudioPhotoInnerTitle().Text;
        }
    }
}
