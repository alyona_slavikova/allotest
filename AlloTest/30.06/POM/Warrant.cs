﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06.POM
{
    public class Warrant
    {

        private IWebDriver _driver;
        public By FourteenDaysRrefundbtn = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/ul/li[2]/a");
        public By WarantyMainLabel = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/section/div[1]/h2");
        public By FourteenDaysMainLabel = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/section/div[2]/div/h2[1]");
       
        public Warrant(IWebDriver driver)   
        {
            this._driver = driver;
        }
        public IWebElement FindWarrantyTitle()
        {
            return _driver.FindElement(WarantyMainLabel);
        }

        public string GetWarrantyTitleText()
        {
            return FindWarrantyTitle().Text;
        }

        
        public Warrant ClickOn14DaysRefundButton()
        {
            _driver.FindElement(FourteenDaysRrefundbtn).Click();
            return new Warrant(_driver);
        }

        public IWebElement FindFourteenDaysTitle()
        {
            return _driver.FindElement(FourteenDaysMainLabel);
        }

        public string GetFourteenDaysTitleText()
        {
            return FindFourteenDaysTitle().Text;
        }

    }
}
