﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace _30._06.FeatureFile
{
    [Binding]
    public class WarrantyServiceSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public Warrant warranty;

        //[Given(@"Allo website is opened")]
        //public void GivenAlloWebsiteIsOpened()
        //{
        //    driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
        //    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
        //    driver.Navigate().GoToUrl("https://allo.ua/ru/");
        //    driver.Manage().Window.Maximize();
        //    mainPage = new MainPage(driver);
        //    warranty = new Warrant(driver);
        //}

        [When(@"user clicks on Warranty button")]
        public void WhenUserClicksOnWarrantyButton()
        {
            mainPage.ClickOnWarrantyButton();
           

        }

        [When(@"User clicks on Fourteen days exchange and return button")]
        public void WhenUserClicksOnFourteenDaysExchangeAndReturnButton()
        {
            warranty.ClickOn14DaysRefundButton();
        }

        [Then(@"Warranty and service page is opened")]
        public void ThenWarrantyAndServicePageIsOpened()
        {
            string WarrantyLabelElementText = warranty.GetWarrantyTitleText();
            Assert.AreEqual("ГАРАНТИЙНОЕ ОБСЛУЖИВАНИЕ", WarrantyLabelElementText);
        }

        [Then(@"page with information about refund is opened")]
        public void ThenPageWithInformationAboutRefundIsOpened()
        {
            string FourteenDaysRefundLabelElementText = warranty.GetFourteenDaysTitleText();
            Assert.AreEqual("ОБМЕН ИЛИ ВОЗВРАТ ТОВАРА ДО 14 ДНЕЙ", FourteenDaysRefundLabelElementText);
        }

    }
}
