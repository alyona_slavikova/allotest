﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace _30._06.FeatureFile
{
    [Binding]
    public class ContactsSteps

    {
        public IWebDriver driver;
        public MainPage mainPage;
        public Contacts contacts;


        [Given(@"Allo web-page is opened")]
        public void GivenAlloWeb_PageIsOpened()
        {
            driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            contacts = new Contacts(driver);
        }

        [When(@"User clicks on Contact Button")]
        public void WhenUserClicksOnContactButton()
        {
            mainPage.ClickOnContactsButton();
        }

        [Then(@"Contact page is opened and contact information is shown")]
        public void ThenContactPageIsOpenedAndContactInformationIsShown()
        {
            string ContactLabelElementText = contacts.GetContactsPageTitleText();
            Assert.AreEqual("Консультации и заказы по телефонам", ContactLabelElementText);
        }

        

    }
}
