﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace _30._06.Steps
{
    [Binding]
    public class TakeACreditToBuyDeviceSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public Credit credit;

        [Given(@"Allo webpage is open")]
        public void GivenAlloWebpageIsOpen()
        {
            driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            credit = new Credit(driver);
        }

        [When(@"I click on Credit button")]
        public void WhenIClickOnCreditButton()
        {
            mainPage.ClickOnCreditButton();
        }

        
        [Then(@"Page with creit conditions is opened")]
        public void ThenPageWithCreitConditionsIsOpened()
        {
            string CreditLabelElementText = credit.GetCreditPageTitleText();
            Assert.AreEqual("Кредит", CreditLabelElementText);
        }
    }
}
