﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace _30._06.FeatureFile
{
    [Binding]
    public class ChooseItemsForChildrenSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public ItemsForChildren children;

       

        [Given(@"Allo page is open")]
        public void GivenAlloPageIsOpen()
        {
            driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            children = new ItemsForChildren(driver);
        }


        [When(@"User clicks on Items for children")]
        public void WhenUserClicksOnItemsForChildren()
        {
            mainPage.ClickOnItemsForChildrenButton();
        }

        [Then(@"User sees label Goods for children")]
        public void ThenUserSeesLabelGoodsForChildren()
        {
            string ItemsForChildrenLabelElementText = children.GetChildrensPageTitleText();
            Assert.AreEqual("Детские товары", ItemsForChildrenLabelElementText);
        }

    }
}
