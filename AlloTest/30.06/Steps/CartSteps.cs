﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace _30._06.Steps
{
    [Binding]
    public class CartSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public PopUpCart popUpCart;


        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
        }

        [When(@"User clicks on cart")]
        public void WhenUserClicksOnCart()
        {
            popUpCart = mainPage.ClickOnCartButton();
        }
        
        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            
          string CartPopUpElementText = mainPage.GetTextFromCartIsEmptyLabel();
          Assert.AreEqual("Ваша корзина пока пуста.", CartPopUpElementText);
            
        }
    }
}
