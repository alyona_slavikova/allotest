﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace _30._06.Steps
{
    [Binding]
    public class Cut_PriceItemsSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public MarkDown markDown;

        [Given(@"Allo page is opened")]
        public void GivenAlloWebpageIsOpened()
        {
            driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            mainPage = new MainPage(driver);
            markDown = new MarkDown(driver);


        }


        [When(@"I click on Markdown Button")]
        public void WhenIClickOnMarkdownButton()
        {
            mainPage.ClickOnMarkDownBtn();
        }

      
        [Then(@"Outlet page is opened")]
        public void ThenOutletPageIsOpened()
        {
            string MarkDownLabelElementText = markDown.GetMarkDownBtnText();
            Assert.AreEqual("РАСПРОДАЖА УЦЕНЁННЫХ ТОВАРОВ", MarkDownLabelElementText);
        }

        [When(@"I click on TV Audio Photo Button")]
        public void WhenIClickOnTVAudioPhotoButton()
        {
            mainPage.ClickOnTVAudioPhotoBtn();
        }

        [Then(@"A list of TVs and Audio is shown")]
        public void ThenAListOfTVsAndAudioIsShown()
        {
            string TVAudioInnerLabelElementText = markDown.GetTVAudioPhotoInnerText();
            Assert.AreEqual("ТЕЛЕВИЗОРЫ, АУДИО И ФОТО", TVAudioInnerLabelElementText);
        }
    }
}
