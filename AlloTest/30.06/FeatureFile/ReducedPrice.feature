﻿Feature: Buying cut-price item when User is not authorized
	As a User 
	I want to see items with reduced price
	In order to buy device cheaper

	As a user
	I want to see a list of TV and audio items
	In order to buy TV cheaper

	Background: 
	Given Allo page is opened
	When I click on Markdown Button

Scenario: Look through the markdown items
	
	Then Outlet page is opened

Scenario: Open a list of TV and Audio items with reduced price
	
	When I click on TV Audio Photo Button
	Then A list of TVs and Audio is shown
