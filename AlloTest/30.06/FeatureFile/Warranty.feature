﻿Feature: Warranty service
	As a user
	I want to be aware of all the information about warranty service
	In order to get warranty servicing or refund 

	As a user 
	I want to read information about 14 days refund
	In order to return recently purchased device 

Background: 
    Given Allo website is opened
	When user clicks on Warranty button

Scenario: Open warranty and service page
	
	Then Warranty and service page is opened

Scenario: Open 14 days refund
	
	When User clicks on Fourteen days exchange and return button
	Then page with information about refund is opened