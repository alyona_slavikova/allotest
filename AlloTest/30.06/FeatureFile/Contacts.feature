﻿Feature: Contacts
	As a user
	I want to see contact information
	In order to see phone number and call to contact center 


Scenario: Open contacts page
	Given Allo web-page is opened
    When User clicks on Contact Button
	Then Contact page is opened and contact information is shown

