﻿using _30._06.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._06
{
    public class Class1
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public PopUpCart popUpCart;

        [OneTimeSetUp]
        public void CreateDriver()
        {
            ChromeDriver driver = new ChromeDriver(@"C:\Users\User\Downloads\chromedriver_win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
        }

        [SetUp]
        public void OpenMainPage()
        {
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
        }
    }
}
